package com.diro.kyctest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KyctestApplication {

    public static void main(String[] args) {

        System.out.println("hello world");
        SpringApplication.run(KyctestApplication.class, args);
    }

}
